# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  ##### VM 1: ansible command station #####
  config.vm.define "base", primary: true do |base|
    ##### BASE BOX #####
    base.vm.box = "ubuntu/bionic64"
    # base.vm.box = "generic/ubuntu1804" # Sometimes useful to get around vagrant ssh issues
    
    ##### NETWORKING #####
    base.vm.hostname = 'ansible-local-ubuntu-base'
    base.vm.network "private_network", type: "dhcp"

    ##### VIRTUALBOX CUSTOMIZATION ######
    base.vm.provider "virtualbox" do |v|
      v.gui = false
      v.memory = 2048

      # Bidirectional clipboard
      v.customize ["modifyvm", :id, "--clipboard", "bidirectional"]

      # Ubuntu slow-to-boot workarounds: https://github.com/hashicorp/vagrant/issues/11890
      v.customize ["modifyvm", :id, "--uart1", "0x3F8", "4"]
      v.customize ["modifyvm", :id, "--uartmode1", "file", File::NULL]
    end

    ##### PROVISIONING VIA ANSIBLE ######
    # A script that's called before using ansible_local's pip installation functionality
    # Installs and tells VM to use Python 3
    base.vm.provision "shell", path: "py3-setup.sh"
    base.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "ansible-base-playbook.yml"
      ansible.install = true
      ansible.install_mode = "pip"
    end

    ##### FOLDER SHARING ######
    # Windows-host specific workaround for ansible_local provisioner to use the project's ansible.cfg
    base.vm.synced_folder ".", "/vagrant",  mount_options: ["dmode=775,fmode=644"]
  end
end
