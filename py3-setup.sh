#!/usr/bin/env bash

### Purpose ###
# This script is meant to be run before a Vagrant ansible_local provision using pip install method.
# It installs Python 3 and pip3, and sets /usr/bin/python to use Python 3.

### Assumptions ###
# - Running as root
# - Running on Ubuntu (tested on 18.04)

### Function Definitions ###
install_and_default_to_python3() {
  echo "Installing Python 3, and setting python3 as default Python interpreter (Vagrant's ansible_local pip provisioner needs this)"
  apt-get update -qq && apt-get install -yqq python3-pip python3
  update-alternatives --install /usr/bin/python python /usr/bin/python3.6 1
  update-alternatives --set python /usr/bin/python3.6
}

### Script Start ###
# Exit with error if not running as root
if [ `id -u` -ne 0 ]; then
  echo "Script isn't running as root, quitting!"
  exit 1
fi

# Ensure Python 3 is available and set as the default Python interpreter
if [ ! `which python` ]; then
  # No Python interpreter is installed
  install_and_default_to_python3
else
  # A Python interpreter is installed, check the major version
  # Adapted from https://stackoverflow.com/questions/6141581/detect-python-version-in-shell-script
  py_major_version=`python -c 'import sys; version=sys.version_info[:1]; print("{0}".format(*version))'`
  echo "Detected default Python major version: $py_major_version"
  if [ $py_major_version -ne 3 ]; then
    install_and_default_to_python3
  fi
fi

# Update provision timestamp
echo "Updating provisioning timestamp (/etc/vagrant_provisioned_at)"
date > /etc/vagrant_provisioned_at

### Script End ###